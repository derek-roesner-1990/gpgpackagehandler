**** PLEASE NOTE ****

**************************

The content below is outdated as I have not worked on this project for many years. 

Considering cleaning it up in the near future but want to do some reaseach first.


Currently I see each component to be at the following state of completion. ERRORS ARE EXPECTED.

- gpg_package_handler.py - Version 0.7
- run.sh                 - Version 0.8

*****************************************************************************************

GPG Package Handler - An ease of use utility for the GnuPG encryption tool.

GPG Package Handler is useful to someone needing to send or receive an encrypted
file(s) or message(s) as the utility will responsibly guide you through the process of both package encryption and decryption.

 --- USAGE ---

To run the program execute the run.sh within the terminal using the following
command within the installation directory.

./run.sh

When sending a package.

-  The contents to encrypt have been placed within the folder named "gpgpackagehandler/data/sender/to_encrpyt".

- The recipient has used GPG Package Handler or another software using GPG to generate a keypair and has stored their public key at hkp://pool.sks-keyservers.net.
If your intended recipient's public key cannot be found during the sending process contact them to ensure they have actually created their keypair and have stored their public key at hkp://pool.sks-keyservers.net. GPG Package Handler can be used to create this keypair as if a keypair does not already exist within the GPG keyring the user is prompted to create a keypair and the public portion of the keypair is stored at hkp://pool.sks-keyservers.net by default.

- You must know the name and email address associated with the recipient's public key.
  The best way to do this is to talk to the recipient and ask for this information.

-  During the encryption process the public key of the recipient will be exported to file and will be placed within "gpgpackagehandler/data/sender/recipient_public_keys". This public key is useful for future communication with this recipient.

- If encryption is successful the resulting file will be place within the folder "gpgpackagehandler/data/receiver/to_decrpyt".

- Send the resulting encrypted content to the receiver.

When receiving a package.

- The contents to decrypt have been placed within the folder named "gpgpackagehandler/data/receiver/to_decrpyt".

- The sender has used GPG Package Handler or another software using GPG and has stored their public key at hkp://pool.sks-keyservers.net.

- You must know the name and email address associated with the sender's public key.
  The easiest way to do this is to talk to the sender and ask for this information.

- During the decryption process you are required to export the public key of the sender to file.
The sender's public key will be placed within "gpgpackagehandler/data/receiver/sender_public_keys".

- If decryption is successful the resulting file will be placed within the folder "gpgpackagehandler/data/receiver/decrypted".

 --- Technical Notes --- 

GPG Package Handler is developed for Ubuntu Linux and Mac OSX.

GPG Package Handler is written in Python and relies on the GnuPG Encryption Tool to manage the encryption and decryption of content. Exchange of messages between sender and receiver rely on the use of a public key cipher to handle exchange of encryption keys and the GnuPG recommended encryption algorithm of RSA is used to encrypt and decrypt the packages it handles. 

GPG Package Handler is built using the Python module python_gnupg which is maintained by Vinay Sajip.

Many thanks to Vinay and all contributors to this module! :) 

Vinay Sajip's GitHub Account 
 - https://github.com/vsajip
 
python_gnupg on GitHub
  - https://github.com/vsajip/python-gnupg

Contributers to python_gnupg 
- https://github.com/vsajip/python-gnupg/graphs/contributors

- https://bitbucket.org/vinay.sajip/python-gnupg/commits/all?page=1
							 
GPG Package Handler is launched via the accompanying script named run.sh, which is written in BASH. When run.sh is executed it will also check for the following resources on the system and if any are not found it will guide the user through the installation process for this particular resource.

Python 3 Interpreter

GnuPG Encryption Tool

Homebrew Package Manager ( Macintosh OSX only )

apt-get Package Manager ( Linux only )

Python Module Manager "Pip3"

Python Module "python-gnupg"

XCode ( Macintosh OSX only )

The C Compiler "gcc"

- Python 3 Interpreter
Allows the instructions defined within gpg_package_handler.py to be read and performed.

- GnuPG Encryption Tool
Handles the encryption and decryption of content and management of encryption keys. 

- Homebrew Package Manager ( Macintosh OSX only )
Handles the installation and uninstallation of the Python3 interpreter and the GnuPG Encryption Tool. 
When installing the Homebrew Package Manager an install of Python Module Manager Pip3 is also installed on the system. 

- apt-get Package Manager ( Linux Only )
Handles the installation and uninstallation of the Python3 interpreter and the GnuPG Encryption Tool.

- Python Module Manager "Pip3"
Handles the installation and uninstallation of Python Module "python-gnupg".
Pip3 is included in Python3 distributions with versions greater than or equal to 3.4.

- Python Module "python-gnupg"
Allows for communication of GPG Package Handler with the GPG Encryption Tool via Python.

- Xcode ( Macintosh OSX only )
Allows for the installation of the XCode Command Line Tools which include an installation of the C Compiler "gcc".

- The C Compiler "gcc"
Allows for the installation of Homebrew Package Manager to be completed. 
gcc will likely be preinstalled if you are running a Linux Distribution. 

For more information on the GnuPG encryption suite, the encryption algorithms used by 
this utility and the public key ciphers please the following resources below. 

--- GnuPG ---

Official Manual            - https://www.gnupg.org/documentation/manuals/gnupg.pdf

GnuPG Privacy Handbook     - https://www.gnupg.org/gph/en/manual.pdf

Frequently Asked Questions - https://gnupg.org/faq/gnupg-faq.html

GPG 'man' page 	           - https://www.gnupg.org/documentation/manpage.html

Copies of the documents can also be found for offline viewing within the folder GPG Documentation found at GPG_Package_Handler/Documentation/GPG Documentation. It is recommended to use the online sources to ensure you are referencing updated information but I will be maintaining this list as the documents are updated and posted publicly.

--- RSA Cryptography ---

Wikipedia - RSA Cryptosystem
https://en.wikipedia.org/wiki/RSA_(cryptosystem)

Duck Duck Go Search Result - RSA Cryptosystem 
https://duckduckgo.com/RSA_(cryptosystem)?ia=web

The Mathematics of the RSA Public-Key Cryptosystem
Burt Kaliski - RSA Laboratories 
http://www.mathaware.org/mam/06/Kaliski.pdf

Cryptography Academy - The RSA Cryptosystem
https://cryptographyacademy.com/rsa/


***************************
 
- Public Key Ciphers -

Wikipedia - Public-key cryptography
https://en.wikipedia.org/wiki/Public-key_cryptography

Duck Duck Go Search Result - Public key encryption
https://duckduckgo.com/?q=public+key+encryption&t=ffsb&ia=web

GnuPG Privacy Handbook - Public-key ciphers
https://www.gnupg.org/gph/en/manual/x195.html

CGI - Public Key Encryption and Digital Signature: How do they work?
https://www.cgi.com/files/white-papers/cgi_whpr_35_pki_e.pdf

Thanks :)
