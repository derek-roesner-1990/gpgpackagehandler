#!/bin/bash

#will install rmtree which allows uninstallation of brew packages
#usage brew rmtree <package>
#brew leaves can be used to show currently installed packages

brew tap beeftornado/rmtree && brew install brew-rmtree