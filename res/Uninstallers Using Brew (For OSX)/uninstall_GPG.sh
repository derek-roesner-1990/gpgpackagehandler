#!/bin/bash

#removing gpg via brew
printf "Packages present before uninstallation of GPG.\n"
brew list

printf " -- Press enter to uninstall GPG -- \n"
read -p "" input  
	
brew rmtree gnupg2
brew rmtree pinentry
brew rmtree libgpg_error
brew rmtree pth
brew rmtree libksba
brew rmtree libassuan
brew rmtree libgcrypt

printf "Packages present after uninstallation of GPG"
brew list
