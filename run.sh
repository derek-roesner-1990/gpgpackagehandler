#!/bin/bash

# To the best of my knowledge this script does not support being called from another script.
# This is untested, but I do not believe startPath will resolve to the intended path.
# startPath is needed to call the python script containing GPG package handler.

##
# Tasks - Order of appearance determines priority
	# 
	#
	# handle support for handle_error() throughout script
	#
	# installation process for linux -
	#   
	#   how to handle keeping supported programs current, if they are already found, add an option??? - apt-get upgrade
    #  
	#
	# ensure Pip3 is installed, 
	#	
	#	MacOSX	
	#	if GPG and Python are already installed and Homebrew is not installed this excludes Pip3
	#	look for Pip3, if not found look for Homebrew, if homebrew found reinstall homebrew, if not found install Pip3
	#	
	#	Linux
	#	use sudo apt-get install python pip3
	# 
	# Improve python installation - unable to be called after installed with Brew.
	#	recommend this option if installation of python3 fails. 	
	#	not linked resulting not being placed on path 
	#   recall brew install python3
	#   if return is python is already installed but not linked recommend below command
	# 	brew link --overwrite --dry-run python to check why its not linked
	#
	# test that PWD resolves to home users directory when scripts are clicked on Linux
	# 
	# offline installation
	#
##

#gets the last folder on the absolute path of the current working directory
#name is print working directory trimmed
#length calculation may be inaccurate based on reading characters or bytes
#pwd_t() {

#	string="$(pwd)"
	
	#len=${#string}  
	#http://stackoverflow.com/questions/17368067/length-of-string-in-bash/17368090#17368090
#	lenth=$(printf "%s" "$string" | wc -c)
	
#	for (( i=lenth-1; i>=0 ; --i ))
#	do
		# if the current character is a forward slash
		# based on ${parameter:offset:length}
#		if [ ${string:i:1} == '/' ] 
#		then
#			folder="${string:i+1:lenth-i}"
			#echo "$folder"
#			return
#		fi
#	done
#}

handle_error() {
	command=$1
	output=$2
	
	printf "An error occured while calling %s.\n" "$command"
	if [ $debugFlag == 1 ] && [ ! -z "$output" ]
	then
		printf >&2 " -- Messages related to error -- \n\"$output\"\n"
		#printf >&2 "$output\n"
	fi
}

prompt_install() {
	install_package="$1"
	
	printf " -- Press y or Y to install %s --\n\n" "$install_package"
	#reads a single character from stdin without echoing the user input
	read  -s -n 1 -p "" input 
		
	if [ "$input" != 'Y' ] && [ "$input" != 'y' ]
	then
		printf "\nCancelling installation and exiting program.\n"
		exit -1
	fi	
}

check_for_library() {
	ret_val=$null
	display_name="$1"
	package_name="$2"
	
	printf "\nChecking for installation of %s.\n" "$display_name"
	errors=$("$package_name" --version 2>&1 > /dev/null)
	ret_val=$? 

	#Handles the command not found event to determine if library is installed. 
	if [ $ret_val == $CMD_NOT_FOUND ]
	then
		printf "Unable to find installation of %s.\n\n" "$display_name"
		return 0
	else
		if [ $ret_val != 0 ]
		then
			if [ $debugFlag == 1 ]
			then
				printf "Calling %s --version returned the following error code %s.\n" "$package_name" "$ret_val"

				#if errors contains output
				if [ ! -z "$errors" ]
				then
					#printf "The following error(s) occurred while calling upon %s.\n" "$display_name"
					printf "%s" "$errors"
				fi
			else
				printf "Errors occurred while calling upon %s.\n" "$display_name"
			fi
			exit -1
		fi
		printf "Installation of %s successfully found.\n" "$display_name"
		return 1
	fi	
}

#Used for installation of gpg and python.
#The two required arguments are detailed below:
#display_name - Used to communicate what is being installed.
#install_name - The package installed by Homebrew Package Manager.
#install_name is not required 
# - if missing install_name is assigned the value of display_name.
#	this is done to avoid use of duplicate arguments such as "gpg" "gpg"
install_with_homebrew() {
	
	ret_val=$null
	display_name="$1"
	install_name="$2"
	 
 	#printf "display name: %s\n" "$display_name"
	#printf "install name: %s\n" "$install_name"
 
	if [ $debugFlag == 1 ]
	then
		err_output=$(brew install -v "$install_name" 2>&1 > /dev/null)
	else
		err_output=$(brew install "$install_name" 2>&1 > /dev/null)	
	fi 
	
	ret_val=$?
	if [ $ret_val != 0 ]
	then
		install_cmd="brew install "$install_name""
		handle_error "$install_cmd" "$err_output"
		return 1
	fi
	printf "%s successfully installed.\n" "$display_name"	
	return 0
}

install_with_aptget() {
	ret_val=$null
	err_output=$null 
	display_name="$1"
	package_name="$2"
	 
	printf "Installing packages using apt-get requires 'root' privilages.\nWhen installing %s you may be asked to enter your root password.\n" "$display_name"
	
	err_output=$(sudo apt-get update 2>&1 > /dev/null)
	ret_val=$?

	if [ $ret_val != 0 ]
	then
		handle_error "apt-get update" "$err_output"
		return 1
	fi
	
	printf "Installing %s.\n" "$display_name" 
	if [ $debugFlag == 1 ]
	then
		err_output=$(sudo apt-get install "$package_name" -v)
	else
		err_output=$(sudo apt-get install -y "$package_name" 2>&1 )
	fi	
	
	ret_val=$?
	if [ $ret_val != 0 ]
	then
		install_command="sudo apt-get install \"$package_name\""		
		handle_error "$install_command" "$err_output"
		return 1
	fi
	
	printf "%s successfully installed.\n" "$display_name"	
	return 0
}


#Install XCode Command Line Tools since this includes an installation of the gcc compiler.
install_xcode_cmdlinetools(){
	#check if xcode is installed
	
	ret_val=$null
	
	# -p, --print-path - prints the path of the active developer directory.
	# &>/dev/null suppresses output from both stdout and stderr.
	install_location=$(xcode-select -p  2>/dev/null)
	ret_val=$?
	
	if [ $ret_val == 0 ]
	then
		printf "The XCode Command Line Tools are already installed at:\n%s.\n" $install_location
	elif [ $ret_val == '2' ] # the return value will be 2 if the command line tools are not installed
	then
		printf "Installing XCode Command Line Tools.\n"
		# 2>&1 > /dev/null suppresses non error output while retaining errors in variable err_output
		err_output=$(xcode-select --install 2>&1 > /dev/null)
		ret_val=$?
	
		if [ $ret_val != 0 ]
		then
			printf "Unable to install XCode Command Line Tools.\n"
			if [ $debugFlag == 1 ] && [ ! -z "$err_output" ]
			then
				printf >&2 " -- Errors messages output while installing XCode Command Line Tools -- \n"
				printf >&2 "$err_output\n"
			fi
			return 1
		fi
		printf "XCode Command Line Tools have been successfully installed."
		
	else
		printf "An error occurred while calling upon xcode-select.\n"
		return 1
	fi
	return 0
}  	

install_homebrew() {
	# may be needed
	# add to path by modifying ~/.profile file 
	# related - Consider setting your PATH so that /usr/local/bin occurs before /usr/bin. 
	# Here is a one-liner:  export PATH=/usr/local/bin:/usr/local/sbin:$PATH
	
	ret_val=$null
	
	# from brew docs - http://docs.brew.sh/Installation.html 
	printf "Installing Homebrew Package Manager\n"
	err_output= $(/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 2>&1 > /dev/null) 
	ret_val=$?
	
	if [ ret_val != 0 ]
	then
		printf "Unable to install Homebrew Package Manager.\n"
		if [ $debugFlag == 1 ] && [ ! -z "$err_output" ]
		then
			printf >&2 " -- Errors messages output while installing Homebrew Package Manager -- \n"
			printf >&2 "$output\n"
		fi	
		return 1
	fi
	
	if [ $debugFlag == 1 ] 
	then
		printf "Modifying path to allow packages installed by Homebrew to be found before system installed packages\n" 
	fi
	#places entry 'export PATH="/usr/local/bin:$PATH' within bash_profile
	#this ensures /usr/local/bin is placed on the path first when bash is started
	echo 'export PATH="/usr/local/bin:$PATH"' >> ~/.bash_profile
	
	printf "Homebrew Package Manager has been successfully installed.\n"
	return 0
}

check_for_python_gnupg() {
	
	ret_val=$null
	package_list=$null
	
	printf "\nChecking for installation of Python resource python-gnupg.\n"
	#More information on python_gnupg can be found here - https://pythonhosted.org/python-gnupg/
	#https://bitbucket.org/vinay.sajip/python-gnupg https://github.com/vsajip/python-gnupg
	
	#may be useful for offline_installation
	#pygnupg_path=/Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/site-packages/python_gnupg-0.4.0-py3.5.egg-info
	
	#if python_gnupg exists within the list of installed python packages
	
	package_list=$(pip3 list  2> /dev/null)

	#find python-gnupg returning only the part of the matching line that contains pattern 'python-gnupg'
	output=$(grep  -o "python-gnupg" <<< "$package_list")
	if [ -z "$output" ]
	then
		printf "Unable to find installation of python-gnupg.\n\n"
		return 0
		
		#install_python_gnupg replaces the commands below
		#command below may be used for offline mode
		#cd "$startPath"
		#cd "res/python-gnupg-0.4.0"
		#python3 setup.py install  
		#printf "python-gnupg library successfully installed.\n"
		
	else
		printf "Installation of python-gnupg successfully found.\n"
		return 1
	fi
}

install_python_gnupg() {
	
	ret_val=$null
	
	#check for pip3
	#if installing on linux use apt-get
	#if installing on mac osx
	#	if GPG and Python are already installed and Homebrew is not installed this excludes Pip3
	#	look for Pip3, if not found look for Homebrew, if homebrew found reinstall homebrew, if not found install Pip3


	printf "Installing packages using pip3 requires 'root' privilages.\nWhen installing %s you may be asked to enter your root password.\n" "$display_name"	
	printf "Installing python-gnupg via Pip3.\n"	
	err_output="$(sudo pip3 install python-gnupg 2>&1 > /dev/null)"		
	ret_val=$?
	
	if [ $ret_val != 0 ]
	then			
		handle_error "sudo pip3 install python-gnupg" "$err_output"
		return 1
	fi
	printf "Python-gnupg successfully installed.\n"	
	return 0
}

# Sets the following variables defined in main. Since these variables are needed in main there is no use redefining them here.  
# debugFlag    - If value is 1 print error/descriptive messages.
# pathToScript - The path leading to the folder containing this script. 
#				(Used if run.sh is launched outside of folder containing gpg_package_handler.py)
parse_commandline_arguments() {
	
	usage="Usage: Path To GPG Package Handler (text string) and/or Debug Flag (0 or 1)"
	debug_flag_err="Invalid argument provided for Debug Flag."
	file_path_err="The provided path to GPG Package Handler does not point to a folder that exists."
	
	check_debug_flag () {
		output="$(grep -e '[0-1]' <<< "$1")"
		
		#if the input for debugFlag is not a zero or 1
		if [ -z "$output" ]
		then
			printf >&2 "%s\n" "$debug_flag_err"
			printf >&2 "%s\n" "$usage"
			exit -1
		fi	
		debugFlag="$1"
	}
	
	check_path_to_script () {
		pathToScript="$1"
		
		# If path to script is a symbolic link
		if [ -L "$pathToScript" ]
		then
			pathToScript="$(readlink "${BASH_SOURCE[0]}")"
		fi
		
		# If pathToScript does not lead to a existing directory	
		if [ ! -d "$pathToScript" ]
		then
			printf >&2 "%s\n" "invalid_file_path_err"
			pathToScript=$null
			exit -1
		fi
	}
	
	# If the first optional argument exists
	if [ ! -z "$1" ]
	then
		# Specific cases would need to be used if parsing more than two arguments was necessary
		case "$1" in
			"/"*|"~"*)
				# If the first character is '/' or '~' it is assumed to be an argument for pathToScript
				check_path_to_script "$1"
				if [ ! -z "$2" ] 
				then
					check_debug_flag "$2"
				fi
			;;
			*)
				# If the first character is not '/' or '~' the argument is assumed to represent debugFlag
				check_debug_flag "$1"
				if [ ! -z "$2" ] 
				then
					check_path_to_script "$2"
				fi
			;;
		esac
	fi
		
	if [ $debugFlag == 1 ]
	then
		printf >&2 "\nArguments Provided\ndebugFlag: %s\npathToScript: %s\n" "$debugFlag" "$pathToScript"
	fi
}

#determining the path leading to gpp_package_handler.py based on how the script was launched.
determine_start_path() {
	# Determining the directory bash is currently navigated to.
	# If this script is launched by being double clicked dirname will resolve to the directory 
	# containing the script or to the directory pointed to by the symlink pointing to the script.
	start_path="$( dirname "${BASH_SOURCE[0]}" )"

	# Messages are printed to stderr as stdout is used to return start_path
	#printf >&2 "\n%s" "$start_path"
	#printf >&2 "\n%s\n" "${BASH_SOURCE[0]}"

	# Detecting if a symlink was used to launch the script.
	if [ -L "${BASH_SOURCE[0]}" ]
	then
		# Read link and then retrieve the directory pointed to by the symlink.
		link_path="$(readlink "${BASH_SOURCE[0]}")"
		start_path="$(dirname "$link_path")"	
	fi

	#printf >&2 "\n%s\n" "$start_path"

	#If the relative directory containing the launched script resolves to the current directory '.'
	#This occurs when the script is launched from the directory it resides within using terminal. 
	if [ "$start_path" == "." ]
	then
		 #retrieving the directory containing the script
		 start_path="$(pwd)"	
	fi
	printf "%s" "$start_path"
}

# -------- MAIN --------

#dirname "$0" 				 #the directory on the relative path used to launch the script
#echo "${BASH_SOURCE[0]}" 	 #the relative path used to launch the script
#dirname "${BASH_SOURCE[0]}" #the directory on the relative path used to launch the script
#pwd 

#camelCase is used for variables defined outside of functions

#pythonLibPath="$startPath/res/Python-3.6.0"
CMD_NOT_FOUND=127	#when a "command not found" event is thrown the return code 127 is returned	
startPath=$null
debugFlag=0
pathToScript=$null
output=$null
ret_val=$null

parse_commandline_arguments "$1" "$2"
#determining execution environment
kernal="$(uname -s)"

#if path to script is not null
if [ ! -z "$pathToScript" ]
then
	startPath="$pathToScript"
else
	startPath="$(determine_start_path)"
fi

printf "\nGPG Package Handler relies on the following pieces of software: \n - C Compiler \"gcc\"\n - Homebrew Package Manager (For installing GPG and Python3)\n - GnuPG Encryption Utility\n - Python3\n - Python3 Package Manager \"pip3\" \n - Python Library \"python-gnupg\"\n - SRM \"Secure Remove\"\n"

#check if gcc compiler is installed.
display_name="C compiler \"gcc\""
package_name="gcc"

check_for_library "$display_name" "$package_name" 
ret_val=$?
if [ $ret_val == 0 ]
then
	printf "The compiler \"gcc\" must be installed in order to install the upcoming software.\n"
	prompt_install "$display_name"
	printf "Installing %s.\n" "$display_name"
	
	#if not and if using OSX, install XCode command line tools since the install includes the gcc compiler.
	if [ "$kernal" == 'Darwin' ]
	then
		install_xcode_cmdlinetools
	fi
	
	#if not and if using a Linux distribution, install using the apt-get package manager
	if ["$kernal" == 'Linux' ]
	then
		install_with_aptget "$package_name"
	fi
	
	ret_val=$?
	if [ $ret_val != 0 ]
	then
		exit -1
	fi	
fi

# If running OSX check if Homebrew Package Manager is installed.
# Homebrew is only used on Mac OSX as on Linux systems apt-get package manager is used instead.
if [ "$kernal" == 'Darwin' ]
then
	display_name="Homebrew Package Manager"
	package_name="brew"
	check_for_library "$display_name" "$package_name"
	ret_val=$?
	
	if [ $ret_val != 0 ]
	then
		printf "Unable to find an installation of the Homebrew Package Manager.\n\n"
		printf "Homebrew must be installed as it is the package manager used to install GnuPG and Python3.\n"
		printf "Homebrew's installation package also includes the Python package manager Pip3 used to install python-gnupg.\n"
		prompt_install "$display_name" 
		
		install_homebrew
		ret_val=$?
		
		#install_homebrew returns 1 if installation fails
		if [ $ret_val != 0 ]
		then
			exit -1
		fi		
	fi
fi


display_name="The Gnu Privacy Guard"
package_name="gpg2"

check_for_library "$display_name" "$package_name"
ret_val=$?

if [ $ret_val == 0 ]
then
	printf "GnuPG must be installed as it is used to encrypt the package being managed.\n"
	prompt_install "$display_name"
		
	if [ "$kernal" == 'Darwin' ]
	then
		printf "Installing %s.\n" "$display_name via Homebrew Package Manager"
		install_with_homebrew "$display_name" "$package_name"	
	fi

	if ["$kernal" == 'Linux' ]
	then
		package_name = "gnupg2"
		install_with_aptget "$display_name" "$package_name"
	fi
	
	ret_val=$?
	if [ $ret_val != 0 ]
	then
		exit -1
	fi	
fi

display_name="Python"
package_name="python3"

check_for_library "$display_name" "$package_name"
ret_val=$?
if [ $ret_val == 0 ]
then
	printf "Python must be installed as GPG Package Handler is written in Python.\n"
	prompt_install "$display_name"
			
	if [ "$kernal" == 'Darwin' ]
	then
		printf "Installing %s.\n" "$display_name via Homebrew Package Manager"
		install_with_homebrew "$display_name" "$package_name"	
	fi	
	
	if [ "$kernal" == 'Linux' ]
	then
		install_with_aptget "$display_name" "$package_name"
	fi
	
	ret_val=$?
	if [ $ret_val != 0 ]
	then
		exit -1
	fi	
fi

display_name="Python Module Manager \"pip3\""
package_name="pip3"
check_for_library "$display_name" "$package_name"
ret_val=$?

if [ $ret_val == 0 ]
then
	printf "The Python module manager \"pip3\" is used to install python-gnupg.\n"
	prompt_install "$display_name"

	if [ "$kernal" == 'Linux' ]
	then		
        package_name="python3-pip"
		install_with_aptget "$display_name" "$package_name" 
	fi

	if [ "$kernal" == 'Darwin' ]
	then
		#look for Homebrew, if homebrew found reinstall homebrew since the current installation includes pip3
		#if homebrew is not found install Pip3. Pip3 cannot be installed when homebrew is installed since this will cause path conflicts
		
		display_name="Homebrew Package Manager"
		package_name="brew"
		check_for_library "$display_name" "$package_name"
		ret_val=$?		

		if [ ret_val == 1 ]
		then
			install_homebrew
		else
			install_pip
		fi
	fi
	
	ret_val=$?

	if [ $ret_val != 0 ]
	then
		exit -1
	fi
	
	output=$(pip3 install --upgrade pip 2>&1)
	
	ret_val=$?	

	if [ $ret_val != 0 ]
	then
		handle_error "pip3 install --upgrade pip" "$output"
		#exiting is not performed if updating pip3 cannot be completed since this is not a fatal error
	fi
fi

display_name="python-gnupg"

#since existence of python-gpg is not detected using --version, check_for_library is not used.
check_for_python_gnupg 
ret_val=$?

if [ $ret_val == 0 ]
then
	printf "The library python-gnupg is used to communicate with GPG via Python.\n"
	prompt_install "$display_name"
	install_python_gnupg
	
	ret_val=$?
	if [ $ret_val != 0 ]
	then
		exit -1
	fi
fi

display_name="SRM - Secure Remove"
package_name="srm"

printf "\nChecking for installation of %s.\n" "$display_name"
errors=$("$package_name" --version 2>&1 > /dev/null)
ret_val=$? 

#Handles the command not found event to determine if library is installed. 
if [ $ret_val == $CMD_NOT_FOUND ]
then
	printf "Unable to find installation of %s.\n\n" "$display_name"
	printf "SRM enables secure deletion of unencrypted file(s) after encryption is complete.\n"
	prompt_install "$display_name"
		
	#if [ "$kernal" == 'Darwin' ]
	#then
	#	install_with_homebrew "$display_name" "$package_name"	
	#fi

	if [ "$kernal" == 'Linux' ]
	then
		package_name="secure-delete"
		install_with_aptget "$display_name" "$package_name"
	fi
	
	ret_val=$?
	if [ $ret_val != 0 ]
	then
		exit -1
	fi
else
	printf "Installation of %s successfully found.\n" "$display_name"	
fi



printf "\nThe required resources have been found.\n"
printf "GPG Package Handler will now run.\n" 
printf "\nGPG Package Handler\nAn ease of use utility for the encryption tool Gnu Privacy Guard.\nUseful to someone needing to send or receive an encrypted file or message.\n\n"
read -p  "-- PRESS ENTER TO BEGIN --" input

#navigating to the directory ./run.sh is launched from.
#this script must be ran from the same folder containing gpg_package_handler.

cd "$startPath"
if [ $debugFlag == 1 ]
then
	printf "\nstartPath: %s\n" "$startPath"
fi

#$HOME is an environmental variable pointing to the users "HOME" directory. 
#By default the home directory will contain configuration files for the current gpg installation.
command="python3 gpg_package_handler.py -l "$HOME""
$command 
exit 1






#install_library function not worked as intended.
#I opted for using homebrew package manager as it manages uninstallation better than installing from source.
#Will be useful for offline installation.
#install_library() {
#	output=$null
#	errors=$null
#	ret_val=$null
#	libPath="$1"
#	sudo_flag=0
#	
#	if [ ! -z "$2" ]
#	then
#		sudo_flag="$2"
#	fi
#	
#	cd "$lib_path"
#	
#	library="$(pwd_t)"
#	printf "Installing library %s.\n" "$library"
#	#pwd
#	
#	# 2>&1 > /dev/null suppresses non error output while retaining errors in variable output
#	output=$(./configure 2>&1 > /dev/null)
#	#output=$(grep -v "warning" <<< "$output")
#	
#	#if debug messages are being printed and messages have been printed to stderr
#	#debugFlag is defined in main as a global variable
#	if [ $debugFlag == 1 ] && [ ${#output} != 0 ]
#	then
#		printf >&2 " -- Messages output to stderr during configure --\n"
#		printf >&2 "$output\n"
#	fi
#	
#	output+=$(make 2>&1 > /dev/null)
#	#capturing the status returned from make
#	ret_val=$?
#	
#	#output=$(grep -v "warning" <<< "$output")
#	
#	# A status of 2 will be returned if any errors were encountered during the call to make.
#	# If configure fails to successfully prepare the library errors will be thrown during make.
#	
#	if [ $ret_val == 2 ]
#	then
#		if [ $debugFlag == 1 ] && [ ${#output} != 2 ]
#		then
#			printf  >&2 " -- Messages output to stderr during Make --\n"
#			printf  >&2 "$output\n"
#		fi
#		return 1
#	fi
#	
#	if [ $sudo_flag == '1' ]
#	then
#		printf "Installing as sudo\n"
#		output+=$(sudo make install 2>&1 > /dev/null)
#	else
#		output+=$(make install 2>&1 > /dev/null)
#	fi
#	
#	ret_val=$?
#	#output=$(grep -v "warning" <<< "$output")
#	
#	if [ $ret_val == 2 ]
#	then
#		if [ $debugFlag == 1 ] && [ ${#output} != 0 ]
#		then
#			printf >&2 " -- Messages output to stderr during Make Install -- \n"
#			printf >&2 "$output\n"
#		fi
#		return 1
#	fi
#	
#	printf "Library %s successfully installed.\n" "$library"
#}


#install_GPG function did not work as intended
#gpg library was not properly installed likely due to permission errors and missing links to resources
#had errors with permission and likely linking.
#install_GPG () {
#	printf "Installing GPG and required libraries.\n"

#	lib_path=$null
#	errors=$null
#	ret_val=$null
#	sudo_flag=1
	
#	#defined in main as global variable.
#	echo "$startPath"	
#	
#	lib_path="$startPath/res/GPG_Libraries/libgpg-error-1.27"	
#	install_library $lib_path
#	ret_val=$?
#	
#	if [ $ret_val == 1 ]
#	then 
#		printf "Unable to install library %s.\n" "$lib_path"
#		return 1
#	fi
#	
#	lib_path="$startPath/res/GPG_Libraries/libgcrypt-1.7.6"
#	install_library $lib_path 
#	ret_val=$?
#	
#	if [ $ret_val == 1 ]
#	then
#		printf "Unable to install library %s.\n" "$lib_path" 
#		return 1
#	fi
#	
#	lib_path="$startPath/res/GPG_Libraries/libassuan-2.4.3"
#	install_library $lib_path
#	ret_val=$?
#	
#	if [ $ret_val == 1 ]
#	then 
#		printf "Unable to install library %s.\n" "$lib_path"
#		return 1
#	fi
#	
#	lib_path="$startPath/res/GPG_Libraries/libksba-1.3.5"
#	install_library $lib_path
#	ret_val=$?
#	
#	if [ $ret_val == 1 ]
#	then 
#		printf "Unable to install library %s.\n" "$lib_path"
#		return 1
#	fi
#	
#	lib_path="$startPath/res/GPG_Libraries/npth-1.3"
#	install_library $lib_path
#	ret_val=$?
#	
#	if [ $ret_val == 1 ]
#	then 
#		printf "Unable to install library %s.\n" "$lib_path"
#		return 1
#	fi
#	
#	lib_path="$startPath/res/GPG_Libraries/pinentry-1.0.0"
#	install_library $lib_path
#	ret_val=$?
#	
#	if [ $ret_val == 1 ]
#	then 
#		printf "Unable to install library %s.\n" "$lib_path"
#		return 1
#	fi
#		
#	printf "All packages required by GPG are installed.\n"	
#	
#	lib_path="$startPath/res/GPG_Libraries/gnupg-2.1.18"
#	install_library $lib_path $sudo_flag
#	ret_val=$?
#	
#	if [ $ret_val == 1 ]
#	then 
#		printf "Unable to install library %s.\n" "$lib_path"
#		return 1
#	fi
#	
#	#chown root /usr/local/bin/gpg
#	#chmod u+s /usr/local/bin/gpg
#		
#	printf "\nGPG successfully installed.\n"
#}
